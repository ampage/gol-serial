
pub mod implem {
    use std::io;
    use std::io::prelude::*;
    use std::io::BufReader;
    use std::fs::File;
    use std::env;
    use std::path::Path;

    // I can't believe I still have to do this in Rust, FFS.
    fn modulo(x: i32, n: i32) -> i32 {
		((x % n) + n) % n
    }
    
	fn next_state(row: usize, col: usize, rows: usize, cols: usize,
		board: &Box<[u8]>)
		-> u8 {
		let live_neighbours = count_live_neighbours(
			row, col, rows, cols, board);
		if board[row*cols + col] == 1 {
	    	if live_neighbours < 2 { 0 }
	    	else if live_neighbours == 2 || live_neighbours == 3 { 1 }
	    	else if live_neighbours > 3 { 0 }
	    	else { 0 }
		} else {
	    	if live_neighbours == 3 { 1 }
	    	else { 0 }
		}
    }

    fn count_live_neighbours(row: usize, col: usize, rows: usize, cols: usize,
			     board: &Box<[u8]>) -> u32 {
		let r: i32 = row as i32;
		let c: i32 = col as i32;
		let rs: i32 = rows as i32;
		let cs: i32 = cols as i32;
		let mut count = 0;

		// NOTE: upper end of range is exclusive, and can't use <= unlike
		// in C. So must add 1 to upper end...
		for y in -1..2i32 {
	    	for x in -1..2i32 {
				if !(x == 0 && y == 0) {
		    		let m: usize = modulo(r+y, rs) as usize;
		    		let n: usize = modulo(c+x, cs) as usize;
		    		if board[m*cols + n] == 1 {
						count += 1;
		    		}
				}
	    	}
		}
		count
    }

    // We don't do array-swapping, but that's not the point of the exercise
	fn update_board(rows: usize, cols: usize, board: &mut Box<[u8]>,
		temp: &mut Box<[u8]>) {
		for row in 0..rows {
	    	for col in 0..cols {
				temp[row*cols + col] = next_state(row, col, rows, cols, board);
	    	}
		}
		board.copy_from_slice(&temp);
		// Have to set temp to zero since we couldn't allocate it within
		// this function
		for i in 0..rows*cols {
	    	temp[i] = 0u8;
		}
    }

    fn print_board(rows: usize, cols: usize, board: &Box<[u8]>) {
		for row in 0..rows {
	    	for col in 0..cols {
				if board[row*cols + col] == 1u8 { print!("@ "); }
			else { print!(". "); }
	    	}
	    	println!("");
		}
    }

	// Returns the boxed slice as an owned value, i.e. ownership is transferred
	// to the caller.
    pub fn init_board_from_file(filename: &Path)
				-> (usize, usize, Box<[u8]>) {
		let project_root = env::current_dir().unwrap();
		let path = project_root.join(Path::new(filename.to_str().unwrap()));

		let file = File::open(path.clone());
		assert!(file.is_ok(), "Cannot open file {:?}", path);
		let file = file.unwrap();

		let reader = BufReader::new(file);

		let mut width: usize = 0;
		let mut height: usize = 0;

		let mut board: Vec<u8> = Vec::new();

		for (row, line) in reader.lines().enumerate() {
			let line = line.expect("Failed to read line");
			if row == 0 {
				width = line.len();
			} else {
				assert!(line.len() == width, "Lines are not all same length");
			}
			for ch in line.chars() {
				if ch == '@' {
					board.push(1u8);
				} else {
					board.push(0u8);
				}
			}
			height += 1;
		}

		assert!(board.len() == width*height);

		(height, width, board.into_boxed_slice())
    }

    pub fn run(rows: usize, cols: usize, board: &mut Box<[u8]>) {
		let mut input = String::new();
		let mut temp_board: Box<[u8]> = vec![0; rows*cols].into_boxed_slice();
		loop {
			print_board(rows, cols, board);
			update_board(rows, cols, board, &mut temp_board);
			
			let bytes_read = io::stdin().read_line(&mut input);
			if bytes_read.expect("Failed to read stdin") == 0 { break; }
		}
    }
}
