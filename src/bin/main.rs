
use std::env;
use std::path::Path;
use gol_serial::implem;

fn main() {
    let args: Vec<String> = env::args().collect();
    assert!(args.len() >= 2, "Must pass filename of initial board config.");
    let filename = Path::new(&args[1]);
    assert!(filename.extension().expect(
      "Board initialisation file must have '.txt' extension") == "txt",
      "Board initialisation file '{:?}' not valid. Must have '.txt' extension.",
      filename);
    
    // The mut keyword can be used independently of the reference operator &.
    // It applies to the binding of board here, and so specifies the nature of
    // our ownership of the board value: we own it mutably.
    let (rows, cols, mut board) = implem::init_board_from_file(&filename);
    
    implem::run(rows, cols, &mut board);
}
