# gol-serial

This is a Rust port of a single-threaded C implementation of Conway's Game of Life.
It's my first project in Rust. I plan to make it multi-threaded at some point, when
I have time.

